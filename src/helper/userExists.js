module.exports = function userExists(ws, name, room = playerRoom[ws.id]) {
	let exists = false;
	for (const id in playerRoom) {
		if (playerRoom[id] !== room || ws.id === id) continue;
		if (names[id] === name) exists = id;
	}
	return exists;
};
