function sendToAll(k, v, ws) {
	const room = playerRoom[ws.id];
	const update = JSON.stringify({
		k,
		v,
	});
	wss.clients.forEach(client => {
		if (playerRoom[client.id] !== room) return;
		client.send(update);
	});
}

function sendToAllExcept(k, v, ws) {
	const room = playerRoom[ws.id];
	const update = JSON.stringify({
		k,
		v,
	});
	wss.clients.forEach(client => {
		if (playerRoom[client.id] !== room) return;
		if (client === ws) return;
		client.send(update);
	});
}

function sendToOne(k, v, ws) {
	ws.send(JSON.stringify({ k, v }));
}

module.exports = { sendToAll, sendToAllExcept, sendToOne };
