const { sendToOne } = require("./send.js");

module.exports = function initNewRoom(ws) {
	positions[playerRoom[ws.id]] = {};
	level[playerRoom[ws.id]] = new Array(50)
		.fill(0)
		.map(() => new Array(50).fill(0));
	rooms[playerRoom[ws.id]] = {};
	rooms[playerRoom[ws.id]].owner = ws.id;
	rooms[playerRoom[ws.id]].coowners = [];
	rooms[playerRoom[ws.id]].locked = false;
	rooms[playerRoom[ws.id]].perms = {
		godmode: true,
		noclip: true,
		tp: true,
	};
	sendToOne(
		"chat",
		"You're the first one to join this room, so you're a room operator! Use /help to see a list of things you can do.",
		ws
	);
};
