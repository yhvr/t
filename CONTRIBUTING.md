# Contributing

If you're looking for something to do, check [the to-do board](https://gitlab.com/yhvr/t/-/boards/2336630). Make sure to format files with [Prettier](https://prettier.io/) after you make changes :D
