// Resizing was removed.

// function reRenderLevel() {
// 	id("lvlWidth").innerHTML = level.length;
// 	id("lvlHeight").innerHTML = level[0].length;
// 	id("levelLayer").width = level.length * blockSize;
// 	id("levelLayer").height = level[0].length * blockSize;
// }

// const resizes = {
// 	expand: {
// 		up() {
// 			for (let i in level) level[i].unshift(0);

// 			player.spawnPoint[1]++;
// 			player.startPoint[1]++;
// 			player.y += blockSize;
// 		},

// 		down() {
// 			for (let i in level) level[i].push(0);
// 		},

// 		left() {
// 			level.unshift(new Array(level[0].length).fill(0));
// 			player.spawnPoint[0]++;
// 			player.startPoint[0]++;
// 			player.x += blockSize;
// 		},

// 		right() {
// 			level.push(new Array(level[0].length).fill(0));
// 		},
// 	},
// 	shrink: {
// 		up() {
// 			if (level[0].length > 1) {
// 				for (let i in level) level[i].shift();
// 				player.spawnPoint[1]--;
// 				player.startPoint[1]--;
// 				player.y -= blockSize;
// 			}
// 		},

// 		down() {
// 			if (level[0].length > 1) {
// 				for (let i in level) level[i].pop();
// 			}
// 		},

// 		left() {
// 			if (level.length > 1) {
// 				level.shift();
// 				player.spawnPoint[0]--;
// 				player.startPoint[0]--;
// 				player.x -= blockSize;
// 			}
// 		},

// 		right() {
// 			if (level.length > 1) level.pop();
// 		},
// 	},
// };

// /**
//  *
//  * @param {string} direction direction to expand or shrink level in
//  * @param {boolean} shrink expand (`false`) or shrink (`true`) the level
//  * @param {boolean} sendThru wether it should be sent thru the ws or not
//  */
// function resizeLevel(direction, shrink = false, sendThru = true) {
// 	if ((direction === "up" || direction === "down") && !shrink) {
// 		if (level[0].length >= 50) return;
// 	}
// 	if ((direction === "left" || direction === "right") && !shrink) {
// 		if (level.length >= 50) return;
// 	}
// 	resizes[shrink ? "shrink" : "expand"][direction]();

// 	prevLevel = [];
// 	reRenderLevel();
// 	drawLevel();

// 	if (sendThru) {
// 		send("dirUpdate", {
// 			level,
// 			way: [direction, shrink],
// 		});
// 	}
// }
