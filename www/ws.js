let positions = {};
let lastSent = [0, 0];
let name,
	room,
	color = "#0000FF";
let names = {};
let colors = {};
let meta = {};

const ws = new WebSocket(`ws://${location.host}/`);

ws.onopen = evt => {
	name = prompt("Enter username:");
	room = prompt("Enter room:");
	color = getColor(name);

	id("room").textContent = room;

	send("welcome", { name, room });
};

ws.onmessage = evt => {
	const obj = JSON.parse(evt.data);
	const data = obj.v;
	switch (obj.k) {
		case "positions":
			positions = data;
			break;
		case "blockUpdate":
			const [x, y, id] = data;
			level[x][y] = id;
			break;
		case "mapUpdate":
			level = data;
			drawLevel();
			toStart();
			break;
		case "names":
			names = data;

			for (const name in names) {
				colors[name] = getColor(names[name]);
			}
			break;
		case "chat": {
			let atBottom =
				document.getElementById("chat").scrollTop ===
				document.getElementById("chat").scrollHeight -
					document.getElementById("chat").clientHeight;
			addToChat(data);

			setTimeout(() => {
				if (atBottom)
					document.getElementById("chat").scrollTo({
						top: document.getElementById("chat").scrollTopMax,
					});
			}, 0);

			break;
		}
		case "meta":
			meta = data;
			break;
		case "perms":
			meta.perms = data;
			break;
		case "err_name_taken":
			alert("That name is already taken in the room! Try another name.");
			ws.onopen();
			break;
	}
};

window.setInterval(() => {
	if (lastSent[0] === player.x && lastSent[1] === player.y) return;

	lastSent = [player.x, player.y];
	send("positions", lastSent);
}, 1000 / 30);

function send(k, v) {
	ws.send(JSON.stringify({ k, v }));
}

// CCC OOO L   OOO RRR
// C   O O L   O O RR
// CCC OOO LLL OOO R R

String.prototype.hashCode = function () {
	var hash = 0;
	for (var i = 0; i < this.length; i++) {
		var character = this.charCodeAt(i);
		hash = (hash << 5) - hash + character;
		hash = hash & hash; // Convert to 32bit integer
	}
	return hash;
};

function componentToHex(c) {
	var hex = c.toString(16);
	return hex.length == 1 ? "0" + hex : hex;
}

function rgbToHex(r, g, b) {
	return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}

function getColor(string) {
	"a".ha;
	let hash = string.hashCode();
	let r = (hash & 0xff0000) >> 16;
	let g = (hash & 0x00ff00) >> 8;
	let b = hash & 0x0000ff;

	return rgbToHex(r, g, b);
}
