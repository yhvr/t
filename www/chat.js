const chat = new Vue({
	el: "#chat",
	data: {
		history: [],
		typing: "",
	},
});

function addToChat(msg) {
	chat.history.push(msg);
}

function sendMessage() {
	send("chat", chat.typing);
	chat.typing = "";
	document.activeElement.blur();
}
