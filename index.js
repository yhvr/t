const app = require("express")();
const { static } = require("express");
const { Server } = require("ws");
const shortid = require("shortid");
const { valid, canPlace, sanitize } = require("./validBlocks.js");
const isOp = require("./src/helper/isOp.js");
const userExists = require("./src/helper/userExists.js");
const initNewRoom = require("./src/helper/initNewRoom.js");
const { sendToAll, sendToAllExcept, sendToOne } = require("./src/helper/send");

const room = ws => global.playerRoom[ws.id];

global.rooms = {};
global.playerRoom = {};
global.names = {};
global.ratelimit = {};

global.wss = new Server({ noServer: true });
wss.on("connection", ws => {
	ws.id = shortid.generate();
	ws.on("message", data => {
		const obj = JSON.parse(data);
		if (obj.v === null) return;
		// Discard packets without room
		if (playerRoom[ws.id] === undefined && obj.k !== "welcome") return;
		switch (obj.k) {
			case "positions":
				const [x, y] = obj.v;
				if (isNaN(x) || isNaN(y)) return;

				positions[playerRoom[ws.id]][ws.id] = obj.v;
				break;
			case "room": {
				// ! Clean up old data
				if (playerRoom[ws.id])
					delete positions[playerRoom[ws.id]]?.[ws.id];

				if (typeof obj.v !== "string") obj.v = "main";
				obj.v = obj.v.substring(0, 32);
				if (userExists(ws, names[ws.id], room)) {
					sendToOne("err_name_taken", null, ws);
					return;
				}

				playerRoom[ws.id] = obj.v;

				// * Init new room, if needed
				if (rooms[playerRoom[ws.id]] === undefined) initNewRoom(ws);

				// * Send new data to guy
				ws.send(
					JSON.stringify({
						k: "mapUpdate",
						v: level[playerRoom[ws.id]],
					})
				);

				// TODO optimize so not *all* names are sent.
				sendToAll("names", global.names, ws);
				sendToOne(
					"meta",
					{
						perms: rooms[playerRoom[ws.id]].perms,
						op: isOp(ws),
					},
					ws
				);

				sendToAll(
					"chat",
					`-- ${global.names[ws.id]} joined the room! --`,
					ws
				);
				break;
			}
			case "blockUpdate": {
				const [x, y, id] = obj.v;
				if (x > 50 || y > 50 || x < 0 || y < 0) return;
				if (
					(rooms[playerRoom[ws.id]].locked && !isOp(ws)) ||
					!valid(id) ||
					!canPlace(id, ws) ||
					(ratelimit[ws.id][0] > 24 &&
						ratelimit[ws.id][1] === Math.floor(Date.now() / 1000))
				) {
					sendToOne(
						"blockUpdate",
						[x, y, level[playerRoom[ws.id]][x][y]],
						ws
					);
					return;
				}

				ratelimit[ws.id][0]++;
				if (ratelimit[ws.id][1] !== Math.floor(Date.now() / 1000)) {
					ratelimit[ws.id][1] = Math.floor(Date.now() / 1000);
					ratelimit[ws.id][0] = 0;
				}

				level[playerRoom[ws.id]][x][y] = sanitize(id);

				sendToAll("blockUpdate", [x, y, id], ws);
				break;
			}
			// case "mapUpdate": {
			// level = obj.v;
			// if (level.length !== 50 || level[0].length !== 50) return;

			// sendToAllExcept("mapUpdate", obj.v, ws.id);
			// break;
			// }
			// case "name": {
			// let name = obj.v;
			// hardcap at 25
			// if (typeof name !== "string") name = "bad child";
			// name = name.substring(0, 25);

			// global.names[ws.id] = name;

			// TODO optimize so not *all* names are sent.
			// sendToAll("names", global.names, ws);
			// break;
			// }
			case "chat": {
				if (typeof obj.v !== "string") return;
				if (obj.v === "") return;
				let msg = obj.v;

				if (msg.startsWith("/")) {
					const res = evaluateCommand(msg.substring(1), ws);
					if (res) ws.send(JSON.stringify({ k: "chat", v: res }));
				} else {
					msg = msg.substring(0, 1000);
					sendToAll("chat", `<${global.names[ws.id]}>: ${msg}`, ws);
				}
				break;
			}
			case "welcome": {
				let { name, room } = obj.v;
				// hardcap at 25
				if (typeof name !== "string") name = "bad child";
				name = name.split(" ").join("").substring(0, 25);

				if (typeof room !== "string") room = "main";
				room = room.substring(0, 32);
				if (userExists(ws, name, room)) {
					sendToOne("err_name_taken", null, ws);
					return;
				}

				global.names[ws.id] = name;
				playerRoom[ws.id] = room;
				ratelimit[ws.id] = [0, Math.floor(Date.now() / 1000)];

				// * Init new room, if needed
				if (rooms[playerRoom[ws.id]] === undefined) initNewRoom(ws);

				// * Send new data to guy
				ws.send(
					JSON.stringify({
						k: "mapUpdate",
						v: level[playerRoom[ws.id]],
					})
				);

				// TODO optimize so not *all* names are sent.
				sendToAll("names", global.names, ws);
				sendToOne(
					"meta",
					{
						perms: rooms[playerRoom[ws.id]].perms,
						op: isOp(ws),
					},
					ws
				);

				sendToAll(
					"chat",
					`-- ${global.names[ws.id]} joined the room! --`,
					ws
				);
				break;
			}
		}
	});

	ws.onclose = () => {
		delete positions[playerRoom[ws.id]]?.[ws.id];
		delete playerRoom[ws.id];
		delete names[ws.id];
	};
});

setInterval(() => {
	wss.clients.forEach(client => {
		let tmp = { ...positions[playerRoom[client.id]] };
		delete tmp[client.id];
		client.send(JSON.stringify({ k: "positions", v: tmp }));
	});
}, 1000 / 30);

global.positions = {};
global.level = {};

const server = app.listen(80);
server.on("upgrade", (request, socket, head) => {
	wss.handleUpgrade(request, socket, head, socket => {
		wss.emit("connection", socket, request);
	});
});

app.use(static("www"));

console.log("We're live! ✌️");

function evaluateCommand(command, ws) {
	command = command.split(" ");
	switch (command[0]) {
		case "lock": {
			if (!isOp(ws)) return;
			rooms[playerRoom[ws.id]].locked = true;
			return "Room locked.";
		}
		case "unlock": {
			if (!isOp(ws)) return;
			rooms[playerRoom[ws.id]].locked = false;
			return "Room unlocked.";
		}
		case "reset": {
			if (!isOp(ws)) return;
			level[playerRoom[ws.id]] = new Array(50)
				.fill(0)
				.map(() => new Array(50).fill(0));
			sendToAll("mapUpdate", level[playerRoom[ws.id]], ws);
			return "Room wiped.";
		}
		case "coop": {
			if (!isOp(ws)) return;
			const name = command[1];
			const user = userExists(ws, name);
			if (!user) return `User not found.`;

			rooms[playerRoom[ws.id]].coowners.push(user);
			return `Made ${name} (id ${user}) a co-operator.`;
		}
		case "decoop": {
			if (!isOp(ws)) return;
			const name = command[1];
			const user = userExists(ws, name);
			if (!user) return `User not found.`;

			rooms[playerRoom[ws.id]].coowners = rooms[
				playerRoom[ws.id]
			].coowners.filter(n => n !== user);
			return `Made ${name} (id ${user}) a regular user.`;
		}
		case "toggleperm": {
			const perm = command[1];
			if (!isOp(ws)) return;
			if (perm !== "godmode" && perm !== "noclip" && perm !== "tp")
				return `Permission not found.`;
			rooms[playerRoom[ws.id]].perms[perm] = !rooms[playerRoom[ws.id]]
				.perms[perm];
			sendToAll("perms", rooms[playerRoom[ws.id]].perms, ws);
			return `${
				rooms[playerRoom[ws.id]].perms[perm] ? "Enabled" : "Disabled"
			} ${perm}.`;
		}
		case "help": {
			return `lock -- Lock the room (from building).
unlock -- Unlock the room (from building).
reset -- Resets the room to be completely blank.
coop [name] -- Make [name] a co-operator.
decoop [name] -- Make [name] a regular user again.
toggleperm [godmode|noclip|tp] -- Toggle if regular users can perform the specified action.`;
		}
	}
}
