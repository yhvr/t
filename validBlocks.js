const isOp = require("./src/helper/isOp.js");

const list = [
	-1,
	0,
	1,
	2,
	3,
	5,
	6,
	7,
	8,
	9,
	10,
	11,
	12,
	13,
	14,
	15,
	16,
	17,
	18,
	21,
	22,
	23,
	24,
	25,
	26,
	27,
	28,
	29,
	30,
	31,
	32,
	33,
	34,
	35,
	36,
	37,
	38,
	39,
	40,
	41,
	42,
	43,
	44,
	45,
	46,
];

module.exports.valid = block => list.includes(block) || list.includes(block[0]);
module.exports.canPlace = (block, ws) => {
	if (!(block instanceof Array)) return true;
	switch (block[0]) {
		case -1:
			return isOp(ws);
		default:
			return true;
	}
};

module.exports.sanitize = block => {
	if (block instanceof Array) {
		switch (block[0]) {
			case -1:
				if (typeof block[1] !== "string") block[1] = "main";
				return [-1, block[1].substring(0, 32)];
			case 46:
				if (typeof block[1] !== "string") block[1] = "<invalid>";
				return [46, block[1].substring(0, 1000)];
			case 41:
				if (typeof block[1] !== "number") block[1] = 0;
				if (typeof block[2] !== "number") block[2] = 0;
				return block;
			default:
				return block[0];
		}
	}
	return block;
};
