# ![tini](images/tini_players.png)

Tini is an acronym for "**T** **I**s **N**ot an **I**ncremental", with "t" being the original name of the project.

## Hosting

Tini is written in Node.js, and relies on a webserver to let people connect and play. If you just want to host a server running tini, run the following commands (for your OS). If your operating system isn't listed here, open a merge request adding instructions for it.

### Prerequisites

Tini uses [Node.js v15](https://nodejs.org/).

### macOS

Notice: You need XCode or Developer Tools installed, etc. etc.

```sh
$ git clone https://gitlab.com/yhvr/t.git
$ cd t
$ npm i
$ node .
```

## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md).
